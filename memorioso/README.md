# Memorioso

Juego de memoria en pseint.

## Juego: 
* La computadora muestra todas las cartas sobre la mesa.
* El usuario deberá memorizar las cartas.
* Todas las cartas se voltean para ocultar sus caras.
* Inicia el juego.
* El usuario elige una carta.
* La computadora le muestra la carta elegida.
* El usuario elige una segunda carta.
* La computadora le muestra la segunda carta elegida.
* Si ambas caras coinciden quedarán descubiertas, de lo contrario se muestra por un tiempo las cartas elegidas y luego se ocultan.
* El usuario continuará descubriendo cartas hasta completar el mazo o salir del juego.

