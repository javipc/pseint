

// Juego de memoria
// Javier Martinez


// Mecánica interna.

// El mazo es un arreglo, una lista de cartas (o tarjetas).
// Cada carta contiene 3 datos (índices)...
//   0 es el valor de la carta, 
//   1 indica si es visible, 
//   2 contiene la imagen de la carta.
// El usuario elige "posiciones" de las cartas.
// La posición es la ubicación de la carta en el arreglo (mazo).
// Para comparar las cartas elegidas se compara el valor (índice 0) de la carta.







// ---------------------
// Funciones varias ----
// ---------------------

// reemplaza una texto en una cadena en la posicion indicada.
// @param linea   : cadena, el texto original que será modificado.
// @param palabra : cadena, el texto que se insertará.
// @param posicion: entero, ubicación en la que comenzará el reemplazo.
// @return cadena.
Funcion nuevo_texto <- sobreescribir(linea, palabra, posicion)
	Definir nuevo_texto Como Caracter;
	nuevo_texto <- Subcadena(linea, 0, posicion -1);
	nuevo_texto <- Concatenar(nuevo_texto, palabra);
	nuevo_texto <- Concatenar(nuevo_texto, Subcadena (linea, posicion + Longitud(palabra), Longitud(linea)));
FinFuncion

// Devuelve una cadena de texto con caracteres repetidos.
// @param cantidad_espacios: entero, cantidad de caracteres que tendrá la línea.
// @param letras: cadena, el texto o letra que se repetirá
// @return cadena.
Funcion linea <- relleno_letras (letras, cantidad)
	Definir linea Como Caracter;
	linea <- "";
	Definir contador Como Entero;

	Para contador <- 1 Hasta cantidad Hacer
		linea <- Concatenar (linea, letras);
	FinPara
FinFuncion


// Devuelve una cadena de texto con espacios en blanco.
// @param cantidad_espacios: entero, cantidad de espacios que tendrá la línea.
// @return cadena.
Funcion linea <- espacios_blanco (cantidad_espacios)
	Definir linea Como Caracter;
	linea <- relleno_letras(" ", cantidad_espacios);	
FinFuncion


// Agrega espacios alrededor de un texto para centrarlo.
// @param linea: cadena, el texto original para centrar.
// @param tam  : entero, el límite de tamaño del texto.
// @return cadena.
Funcion linea <- centrar_texto (linea, tam)		
	Si Longitud(linea) < tam Entonces
		Repetir
			linea <- Concatenar (" ", linea);
			linea <- Concatenar (linea, " ");
			linea <- Subcadena (linea, 0, tam);
		Hasta Que Longitud(linea) >= tam;
	FinSi
	linea <- Subcadena (linea, 0, tam-1);
FinFuncion







// ---------------
// Cartas --------
// ---------------

// Verifica si una carta se puede seleccionar, es decir si está oculta (cara abajo).
// El usuario no deberá poder seleccionar cartas visibles (descubiertas).
// @param cartas  : cadena, mazo de cartas.
// @param posicion: entero, ubicación de la carta en el mazo.
// @return lógico.
Funcion seleccionable <- esta_oculta (cartas, posicion)
	Definir  seleccionable Como Logico;	
	seleccionable <- cartas[posicion, 1] <> cartas[posicion, 0];
FinFuncion

// Hace visible una carta dejando su cara arriba.
// @param cartas  : arreglo, el mazo de cartas.
// @param posicion: entero, la posición de la carta en el mazo.
SubProceso hacer_visible_carta(cartas, posicion)
	cartas[posicion, 1] <- cartas[posicion,0];
FinSubProceso

// Oculta una carta dejando su cara abajo.
// @param cartas  : arreglo, el mazo de cartas.
// @param posicion: entero, la posición de la carta en el mazo.
SubProceso hacer_oculta_carta(cartas, posicion)
	cartas[posicion, 1] <- "";
FinSubProceso

// Verifica si todas las cartas son visibles (fin del juego).
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, la cantidad de cartas en el mazo.
// @return lógico.
Funcion son_visibles <- son_todas_visibles(cartas, total_cartas)
	Definir son_visibles Como Logico;
	Definir posicion Como entero;
	son_visibles <- Verdadero;
	Para posicion <- 0 hasta total_cartas -1 Hacer
		Si esta_oculta(cartas, posicion) Entonces
			son_visibles <- Falso;
		FinSi
	FinPara
FinFuncion

// Hace visible todas las cartas.
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, total de cartas en el mazo.
SubProceso hacer_visible_cartas(cartas, total_cartas)
	Definir posicion Como entero;	
	Para posicion <- 0 hasta total_cartas -1 Hacer
		hacer_visible_carta(cartas, posicion);
	FinPara
FinSubProceso

// Oculta todas las cartas.
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, total de cartas en el mazo.
SubProceso hacer_oculta_cartas(cartas, datos_pantalla)
	Definir posicion, total_cartas Como entero;	
	Definir frase Como Caracter;
	frase <- frase_aleatoria();
	total_cartas <- datos_pantalla[0];
	Para posicion <- 0 hasta total_cartas -1 Hacer
		hacer_oculta_carta(cartas, posicion);
		dibujar_pantalla(cartas, datos_pantalla);
		Escribir frase;
		Esperar 120 milisegundos;
	FinPara
FinSubProceso

// Obtiene el valor de una carta.
// @param cartas  : cadena, mazo de cartas.
// @param posicion: entero, ubicación de la carta en el mazo.
// @return caracter.
Funcion carta <- seleccionar_carta (cartas, posicion)
	Definir  carta Como Caracter;
	carta <- cartas[posicion,0];
FinFuncion

// Dadas dos posiciones verifica que las cartas sean iguales.
// @param cartas   : arreglo, el mazo.
// @param posicion1: entero, posición de una carta para comparar.
// @param posicion2: entero, posición de la otra carta para comparar.
// @return lógico.
Funcion igualdad <- son_iguales (cartas, posicion1, posicion2)
	Definir igualdad Como Logico;
	igualdad <- seleccionar_carta(cartas, posicion1) == seleccionar_carta(cartas, posicion2);
FinFuncion


// Intercambia de lugar un par de cartas.
// @param cartas   : cadena, el mazo de cartas.
// @param posicion1: entero, posición de una carta para intercambiar.
// @param posicion2: entero, posición de otra carta para intercambiar por la primera.
SubProceso intercambiar_carta (cartas, posicion1, posicion2)
	Definir  cara, visible, imagen Como Caracter;
	cara    <- cartas[posicion1, 0];
	visible <- cartas[posicion1, 1];
	imagen  <- cartas[posicion1, 2];
	
	cartas[posicion1,0] <- cartas[posicion2,0];
	cartas[posicion1,1] <- cartas[posicion2,1];
	cartas[posicion1,2] <- cartas[posicion2,2];

	cartas[posicion2,0] <- cara;
	cartas[posicion2,1] <- visible;
	cartas[posicion2,2]	<- imagen;
FinSubProceso


// Mezcla un mazo de cartas.
// @param cartas: cadena, un mazo de cartas para mezclar.
// @param total_cartas: entero, la cantidad de cartas en el mazo.
SubProceso mezclar_cartas (cartas, total_cartas)
	Definir contador_mezcla, posicion1, posicion2 Como Entero;
	contador_mezcla <- total_cartas * 100;
	Repetir
		posicion1 <- azar (total_cartas);
		posicion2 <- azar (total_cartas);
		intercambiar_carta(cartas, posicion1, posicion2);
		contador_mezcla <- contador_mezcla -1;
	Hasta Que contador_mezcla == 0;
FinSubProceso


SubProceso completar_info_carta (cartas, carta, posicion, tamx, tamy)
		cartas[posicion, 0] <- carta;
		cartas[posicion, 1] <- carta;
		cartas[posicion, 2] <- crear_imagen_carta (tamx, tamy, carta);

		cartas[posicion +1, 0] <- cartas[posicion, 0];
		cartas[posicion +1, 1] <- cartas[posicion, 1];
		cartas[posicion +1, 2] <- cartas[posicion, 2];
FinSubProceso









// ---------------------
// vista ---------------
// ---------------------

// Creado con figlet 
// http://www.figlet.org/
SubProceso mostrar_titulo ()
	Escribir " __  __                           _  ___  ____   ___";
	Escribir "|  \/  | ___ _ __ ___   ___  _ __(_)/ _ \/ ___| / _ \";
	Escribir "| |\/| |/ _ \  _ ` _ \ / _ \|  __| | | | \___ \| | | |";
	Escribir "| |  | |  __/ | | | | | (_) | |  | | |_| |___) | |_| |";
	Escribir "|_|  |_|\___|_| |_| |_|\___/|_|  |_|\___/|____/ \___/";
	Escribir "";	
FinSubProceso

// Devuelve una cadena que contiene una representación de la imagen de una carta.
// @param tamx : entero, ancho de la carta.
// @param tamy : entero, alto de la carta.
// @param carta: caracter, el valor de la carta.
// @return cadena.
Funcion imagen_carta <- crear_imagen_carta (tamx, tamy, carta)
	Definir imagen_carta, linea Como Caracter;
	imagen_carta <- "";
	
	Definir contadory Como Entero;
	
	contadory <- 0;
		
	// texto de la carta (la cara).
	carta <- centrar_texto (carta, tamx -2);
	
	Repetir
		// Línea en blanco del ancho de la carta.
		linea <- espacios_blanco (tamx -2);
			
		// A la mitad de la carta pone el texto.
		Si redon(tamy / 2) -1 == contadory Entonces
			linea <- carta;
		FinSi
		
		// Línea superior e inferior.
		Si contadory == 0 o contadory == tamy -1 Entonces
			linea <- relleno_letras ("-", tamx -2);
		FinSi
		
		// Líneas laterales.
		linea <- Concatenar ("|", linea);
		linea <- Concatenar (linea, "|");
		
		imagen_carta <- Concatenar(imagen_carta,  linea );

		contadory <- contadory +1;
		
	Hasta Que contadory == tamy;
FinFuncion




// Crea una cadena de texto que representa una pantalla.
// @param cartas: arreglo, un mazo de cartas.
// @param total_cartas: entero, cantidad de cartas en el mazo.
// @param tamx: entero, tamaño de las cartas en eje x.
// @param tamy: entero, tamaño de las cartas en eje y.
// @param cartas_x: cantidad de cartas en eje x.
// @return cadena.
Funcion imagen_pantalla <- crear_imagen_pantalla (cartas, total_cartas, tamx, tamy, cartas_x)
	Definir imagen_pantalla Como Caracter;
	Definir cartas_desde, cartas_hasta, espacios_vacios como Entero;
	imagen_pantalla <- "";
	cartas_desde    <- 0;
	cartas_hasta    <- cartas_x;
	espacios_vacios <- 0;
	Repetir
		imagen_pantalla <- Concatenar (imagen_pantalla, crear_imagen_pantalla_x (cartas, cartas_desde, cartas_hasta,  tamx, tamy, espacios_vacios));
		cartas_desde    <- cartas_desde + cartas_x;
		cartas_hasta    <- cartas_desde + cartas_x;
		Si cartas_hasta > total_cartas Entonces
			espacios_vacios <- (cartas_hasta - total_cartas);
			cartas_hasta    <- total_cartas;
		FinSi;
	Hasta Que cartas_desde >= total_cartas;

FinFuncion

// Crea una imagen con una fila de cartas.
Funcion imagen_pantalla <- crear_imagen_pantalla_x (cartas, cartas_desde, cartas_hasta, tamx, tamy, espacios_vacios)
	Definir imagen_pantalla, imagen, linea, carta_blanca Como Caracter;
	imagen_pantalla <- "";
	carta_blanca    <- crear_imagen_carta (tamx, tamy, " ");

	Definir contador_carta, contadory Como Entero;
	
	contadory <- 0;
	Repetir		
		contador_carta <- cartas_desde;
		Repetir
			// Muestra solo las cartas visibles.
			Si esta_oculta(cartas, contador_carta) Entonces
				imagen <- carta_blanca;
			Sino
				imagen <- cartas[contador_carta, 2];
			FinSi
			
			// De la cadena completa de la imagen obtiene la subcadena que dibuja el eje Y actual.
			linea <- Subcadena(imagen, contadory * tamx, (contadory * tamx) + tamx-1);

			// pone el número ubucación de la carta.
			Si contadory == tamy -2 Entonces
				linea <- sobreescribir(linea, ConvertirATexto(contador_carta+1),2);
			FinSi

			// Agrega la línea alrededor de espacios en blanco para separar una carta de la otra.
			imagen_pantalla <- Concatenar (imagen_pantalla, " ");
			imagen_pantalla <- Concatenar (imagen_pantalla, linea);
			imagen_pantalla <- Concatenar (imagen_pantalla, " ");
			
			
			contador_carta <- contador_carta +1;
		Hasta Que contador_carta >= cartas_hasta;
		
		// Completa espacios vacios en el tablero.
		Si espacios_vacios > 0 Entonces
			imagen_pantalla <- Concatenar (imagen_pantalla, relleno_letras(" ", (espacios_vacios * (tamx+2))));
		FinSi
		contadory <- contadory +1;
	Hasta Que tamy == contadory;
		

FinFuncion


// Imprime en pantalla una imagen.
// @param imagen: cadena, texto que contiene la imagen.
// @param tamx: entero, tamaño en eje x de la imagen.
SubProceso dibujar (imagen, tamx)
	Definir contadory Como Entero;
	contadory <- 0;
	Definir linea Como Caracter;
	Repetir
		linea <- Subcadena(imagen, contadory * tamx, (contadory * tamx) + tamx-1); 
		Escribir linea;
		contadory <- contadory +1;
	Hasta Que linea == "";
FinSubProceso



// Imprime en pantalla las cartas.
// @param cartas: arreglo, el mazo de cartas.
// @param datos_pantalla: arreglo, medidas de las cartas y otros datos.
SubProceso dibujar_pantalla (cartas, datos_pantalla)
	Definir total_cartas, tamx, tamy, cartas_x Como Entero;
	total_cartas <- datos_pantalla[0];
	tamx         <- datos_pantalla[1];
	tamy         <- datos_pantalla[2];
	cartas_x     <- datos_pantalla[3];

	Definir imagen Como caracter;
	imagen <- crear_imagen_pantalla (cartas, total_cartas, tamx, tamy,cartas_x);
	
	Limpiar Pantalla;
	Escribir " / MemoriOSO ///";
	dibujar(imagen, (tamx+2)*cartas_x);

	
FinSubProceso



Funcion frase <- frase_aleatoria ()
	Definir frase Como Caracter;
	Segun azar(10) hacer
	1: frase <- "No creo que lo logres.";
	2: frase <- "No sé si este juego es para vos.";
	3: frase <- "Ahora te quiero ver.";
	4: frase <- "Llegó la hora de la verdad.";
	De otro modo:
		frase <- "Calculando en cuanto tiempo vas a rendirte...";
	FinSegun
FinFuncion

Funcion frase <- noo()
	Definir frase Como Caracter;
	Segun azar(20) hacer
	1: frase <- "¡NOOOOOOO!";
	2: frase <- "¡Que NO!";
	3: frase <- "¡NOOOOOOO! :-D";
	4: frase <- "¡Jajaja!.... ¡NOOOOOOO!";
	5: frase <- "No, no, no y ¡NO!";
	6: frase <- "Pffffff...";
	7: frase <- "Me parece que no";

	De otro modo:
		frase <- "¡NO!";
	FinSegun
FinFuncion


SubProceso mostrar_movimientos (movimientos)
	Escribir "[Movimientos: ", movimientos[0], "  Errores: ", movimientos[1], "]  [Consecutivos: ", movimientos[2], "  Máximo: ", movimientos[3], "]";
FinSubProceso

SubProceso sumar_movimiento (movimientos)
	// suma movimientos
	movimientos[0] <- movimientos[0]+1;
	// suma consecutivos
	movimientos[2] <- movimientos[2]+1;
	Si movimientos[2] > movimientos[3] Entonces
		movimientos[3] <- movimientos[2];
	FinSi
FinSubProceso

SubProceso sumar_error (movimientos)
	// suma el movimiento
	movimientos[0] <- movimientos[0] +1;
	// suma error
	movimientos[1] <- movimientos[1] +1;
	// reinicia consecutivos
	movimientos[2] <- 0;
	
FinSubProceso

SubProceso inicializar_movimientos (movimientos)
	Definir contador Como Entero;
	Para contador <- 0 Hasta 3 Hacer
		movimientos[contador] <-0;
	FinPara
FinSubProceso

// ---------------------
// usuario -------------
// ---------------------


// Usuario selecciona una carta.
// @param cartas: cadena, mazo de cartas.
// @return entero.
SubProceso posicion<- elegir_carta (cartas)
	Definir posicion Como Entero;
	Definir carta Como Caracter;
	
	Repetir
		Escribir Sin Saltar "Seleccionar una carta: ";
		leer posicion;
		Si posicion <= 0 Entonces
			Escribir "¿", ConvertirATexto(posicion), "?";
			posicion <- 1;
		FinSi
		posicion <- posicion -1;
		Si esta_oculta(cartas, posicion) == Falso Entonces
			Escribir "Esa carta o posición no es correcta";
		FinSi
	Hasta Que esta_oculta(cartas, posicion);

	carta <- seleccionar_carta(cartas, posicion);	
	Escribir "Carta seleccionada: ", carta;
FinSubProceso


// Obtiene un mazo de cartas.
// @param cartas: arreglo cadena, mazo de cartas.
// @param total : entero, total de cartas en el mazo.
// @param tamx  : entero, tamaño de la carta en eje x.
// @param tamy  : entero, tamaño de la carta en eje y.
SubProceso ingresar_cartas (cartas, total, tamx, tamy)
	Definir carta, carta_automatica como Caracter;
	Repetir
		carta_automatica <- obtener_valor_aleatorio ();
		Escribir "";
		Escribir "Pares restantes: ", total / 2;
		Escribir "Escribí el valor de una carta (Enter = ", carta_automatica, "): ";
		leer carta;
		Si carta == "" Entonces
			carta <- carta_automatica;
		FinSi
		
		total <- total -2;
		completar_info_carta(cartas, carta, total, tamx, tamy);
		

	Hasta que total == 0;
FinSubProceso



// ----------------
// automático
// ----------------

Funcion valor <- obtener_valor_aleatorio ()
	Definir valor Como Caracter;
	Segun azar(40) hacer
		 1: valor <- "casa";
		 2: valor <- "mapa";
		 3: valor <- "auto";
		 4: valor <- "hola";
		 5: valor <- "pepe";
		 6: valor <- "aire";
		 7: valor <- "yoyo";
		 8: valor <- "kilo";
		 9: valor <- "cero";
		10: valor <- "remo";
		11: valor <- "frio";
		12: valor <- "luna";
		13: valor <- "dado";
		14: valor <- "dama";
		15: valor <- "coma";
		16: valor <- "pino";
		17: valor <- "sala";
		18: valor <- "sapo";
		19: valor <- "mago";
		20: valor <- "isla";
		21: valor <- "chau";
		22: valor <- "puma";
		23: valor <- "amor";
		24: valor <- "mono";
		25: valor <- "chip";
		26: valor <- "zona";
		27: valor <- "nube";
		28: valor <- "rosa";
		29: valor <- "jazz";
		30: valor <- "higo";
		31: valor <- "tres";
		32: valor <- "polo";
		33: valor <- "este";
		34: valor <- "lila";
		35: valor <- "azul";
		36: valor <- "seis";
		37: valor <- "mijo";
		38: valor <- "ocho";
		39: valor <- "rima";
		39: valor <- "fino";
		39: valor <- "fama";

		De otro modo:			
			valor <- obtener_valor_aleatorio();
	FinSegun;
FinFuncion
		
SubProceso obtener_valores_aleatorios (cartas, total)
	Repetir
			
		total <- total -2;
		carta <- obtener_valor_aleatorio();
		completar_info_carta(cartas, carta, total, tamx, tamy);
		
	Hasta que total == 0;
FinSubProceso





// ------------------
// Mecánica de juego.
// ------------------

SubProceso jugar (cartas, datos_pantalla)

		
	Definir total_cartas, posicion1, posicion2, tiempo_espera Como Entero;
	total_cartas <- datos_pantalla [0];
	
	// Contador de movimientos.
	Definir movimientos Como Entero;
	Dimension movimientos(10);
	inicializar_movimientos(movimientos);
	
	Repetir
		// El jugador elige una carta.
		dibujar_pantalla(cartas, datos_pantalla);
		mostrar_movimientos(movimientos);
		posicion1 <- elegir_carta(cartas);
		hacer_visible_carta(cartas, posicion1);		

		// La carta elegida se muetra, el jugador debe encontrar el par.
		dibujar_pantalla(cartas, datos_pantalla);
		mostrar_movimientos(movimientos);
		posicion2 <- elegir_carta(cartas);
		hacer_visible_carta(cartas, posicion2);

		// El par elegido se muestra y se compara.
		dibujar_pantalla(cartas, datos_pantalla);
		Si son_iguales(cartas, posicion1, posicion2) Entonces
			Escribir "Correcto.";
			Esperar 600 milisegundos;
			sumar_movimiento(movimientos);
		SiNo
			sumar_error(movimientos);
			Escribir noo();
			Esperar 1 segundos;
			Para tiempo_espera <- 3 hasta 1 con paso -1 hacer
				dibujar_pantalla(cartas, datos_pantalla);
				Escribir "Continua en...", tiempo_espera;
				Esperar 1 segundos;
			FinPara
			hacer_oculta_carta(cartas, posicion1);
			hacer_oculta_carta(cartas, posicion2);
		FinSi

		// Si no hay más para descubrir...
		Si son_todas_visibles(cartas, total_cartas) Entonces
			Escribir "¡GANASTE!";
		FinSi

		// Una posible opción para abandonar.
		Si posicion2 > total_cartas Entonces
			hacer_visible_cartas(cartas, total_cartas);
			Escribir "adios";
		FinSi

	Hasta Que son_todas_visibles(cartas, total_cartas);
	
FinSubProceso








Proceso MemoriOSO
	Limpiar Pantalla;
	mostrar_titulo();
	Escribir "¡Hola!";
	Escribir "Este es un juego de memoria";
	Escribir "En la pantalla tendrás cartas (o tarjetas) y tu objetivo es encontrar los pares idénticos.";


	Definir total_cartas, tamx, tamy, cartas_x Como Entero;
	Definir tiempo_espera Como Entero;

	// Tamaño de las cartas.
	tamx <- 6;
	tamy <- 5;


	Escribir "Elegí la cantidad de pares de cartas que queres tener en la pantalla. Cuantas más cartas es más dificil.";
	Escribir "Si no sabes que elegir pulsá Enter para jugar con 10 pares.";
	Escribir sin Saltar "Pares de cartas (Enter = 10): ";
	leer total_cartas;
	Si total_cartas <= 0 Entonces
		total_cartas <- 10;
	FinSi

	Limpiar pantalla;
	mostrar_titulo();
	Si total_cartas < 10 Entonces
		Escribir "¡No seas cobarde, ese nivel es para niños!";
	FinSi

	Escribir "";
	Escribir "Ahora hay que acomodar las filas y columnas en la pantalla...";	
	Escribir "Si no sabes que elegir apretá Enter para tener 5 columnas.";
	Escribir sin Saltar "Cantidad de columnas (Enter = 5): ";
	leer cartas_x;
	Si cartas_x == 0 Entonces
		cartas_x <- 5;
	FinSi

	// duplica la cantidad de cartas.
	total_cartas <- total_cartas *2;


	// Mazo
	Definir cartas Como Caracter;
	Dimension cartas(total_cartas,3);

	// Para uso de pantalla.
	Definir datos_pantalla Como Entero;
	Dimension datos_pantalla(10);
	datos_pantalla[0] <- total_cartas;
	datos_pantalla[1] <- tamx;
	datos_pantalla[2] <- tamy;
	datos_pantalla[3] <- cartas_x;

	
	// Se generan o ingresan textos de las cartas.
	Limpiar pantalla;
	mostrar_titulo();
	Escribir "Podes escribir el texto de las tarjetas o presionar Enter para elegir de forma automática.";
	ingresar_cartas(cartas, total_cartas, tamx, tamy);
	

	// Muestra pares elegidos.
	dibujar_pantalla(cartas, datos_pantalla);
	Escribir "Estas son tus cartas elegidas. Ahora a mezclar...";
	Esperar 2 segundos;

	// Mezcla las cartas.
	Para tiempo_espera <- 10 hasta 0 con paso -1 hacer
		mezclar_cartas(cartas, total_cartas);
		dibujar_pantalla(cartas, datos_pantalla);
		Escribir "Estoy mezclando...", tiempo_espera;
		Esperar 20 milisegundos;
	FinPara

	// Muestra todas las cartas para que el jugador las memorice.
	dibujar_pantalla(cartas, datos_pantalla);
	Escribir "Tenes unos pocos segundos para memorizar todo...";
	Esperar 10 segundos;

	// Se termina el tiempo para memorizar...
	Para tiempo_espera <- 5 Hasta 0 con paso -1 Hacer
		dibujar_pantalla(cartas, datos_pantalla);
		Escribir "Empieza en ", tiempo_espera;
		Esperar 1 segundos;
	FinPara
	
	// Ahora oculta todas las cartas.
	hacer_oculta_cartas(cartas, datos_pantalla);

	// Comienza el juego.
	jugar(cartas, datos_pantalla);
FinProceso
