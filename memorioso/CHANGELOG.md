# Memorioso

### 2023.04.27
* Creación de este documento.
* Versión inicial en desarrollo.

### 2023.04.29
* Agregado.   Función que crea una imagen de una carta.
* Modificado. Uso de arreglos en vez de cadenas para representar el mazo de cartas.
* Agregado.   Salida en pantalla (provisorio).

### 2023.04.30
* Agregado.   Vista, salida en pantalla.
* Agregado.   Palabras automáticas.
* Modificado. Pabras de diferente tamaño.
* Agregado.   Contador tiempo.
* Modificado. Mejorado el algoritmo de imágenes.
* Agregado.   Opción de tamaño de tablero.

### 2023.05.01
* Agregado.   Impide elegir carta 0, o pulsar enter sin introducir una carta.
* Agregado.   Números en cada carta.
* Modificado. Más cartas aleatorias.
* Agregado.   Animación al ocultar cartas.
* Agregado.   Frases y textos.
* Modificado. Previsualización de cartas aleatorias antes de elegirlas.

### 2023.05.02
* Agregado.   Texto [figlet](http://www.figlet.org/)

### 2023.05.05
* Agregado    Contador de movimientos.