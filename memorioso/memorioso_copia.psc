

// Juego de memoria
// Javier Martinez


// Mecánica interna.

// El mazo es un arreglo, una lista de cartas.
// Cada carta contiene 3 datos (índices)...
//   0 es el valor de la carta, 
//   1 indica si es visible, 
//   2 contiene la imagen de la carta.
// El usuario elige "posiciones" de las cartas.
// La posición es la ubicación de la carta en el arreglo (mazo).
// Para comparar las cartas elegidas se compara el valor (índice 0) de la carta.





// ---------------------
// Funciones varias ----
// ---------------------

// Devuelve una cadena de texto con espacios en blanco.
// @param cantidad_espacios: entero, cantidad de espacios que tendrá la línea.
// @return cadena.
Funcion linea <- espacios_blanco (cantidad_espacios)
	Definir linea Como Caracter;
	linea <- "";
	Definir contador Como Entero;

	Para contador <- 0 Hasta cantidad_espacios Hacer
		linea <- Concatenar (linea, " ");
	FinPara
FinFuncion



// ---------------
// Cartas --------
// ---------------

// Verifica si una carta se puede seleccionar, es decir si está oculta (cara abajo).
// El usuario no deberá poder seleccionar cartas visibles (descubiertas).
// @param cartas: cadena, mazo de cartas.
// @param posicion: entero, ubicación de la carta en el mazo.
// @return lógico.
Funcion seleccionable <- esta_oculta (cartas, posicion)
	Definir  seleccionable Como Logico;	
	seleccionable <- cartas[posicion, 1] <> cartas[posicion, 0];
FinFuncion

// Hace visible una carta dejando su cara arriba.
// @param cartas: arreglo, el mazo de cartas.
// @param posicion: entero, la posición de la carta en el mazo.
SubProceso hacer_visible_carta(cartas, posicion)
	cartas[posicion, 1] <- cartas[posicion,0];
FinSubProceso

// Oculta una carta dejando su cara abajo.
// @param cartas: arreglo, el mazo de cartas.
// @param posicion: entero, la posición de la carta en el mazo.
SubProceso hacer_oculta_carta(cartas, posicion)
	cartas[posicion, 1] <- "";
FinSubProceso

// Verifica si todas las cartas son visibles (fin del juego).
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, la cantidad de cartas en el mazo.
// @return lógico.
Funcion son_visibles <- son_todas_visibles(cartas, total_cartas)
	Definir son_visibles Como Logico;
	Definir posicion Como entero;
	son_visibles <- Verdadero;
	Para posicion <- 0 hasta total_cartas -1 Hacer
		Si esta_oculta(cartas, posicion) Entonces
			son_visibles <- Falso;
		FinSi
	FinPara
FinFuncion

// Hace visible todas las cartas.
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, total de cartas en el mazo.
SubProceso hacer_visible_cartas(cartas, total_cartas)
	Definir posicion Como entero;	
	Para posicion <- 0 hasta total_cartas -1 Hacer
		hacer_visible_carta(cartas, posicion);
	FinPara
FinSubProceso

// Oculta todas las cartas.
// @param cartas: arreglo, el mazo de cartas.
// @param total_cartas: entero, total de cartas en el mazo.
SubProceso hacer_oculta_cartas(cartas, total_cartas)
	Definir posicion Como entero;	
	Para posicion <- 0 hasta total_cartas -1 Hacer
		hacer_oculta_carta(cartas, posicion);
	FinPara
FinSubProceso

// Obtiene el valor de una carta.
// @param cartas: cadena, mazo de cartas.
// @param posicion: entero, ubicación de la carta en el mazo.
// @return caracter.
Funcion carta <- seleccionar_carta (cartas, posicion)
	Definir  carta Como Caracter;
	carta <- cartas[posicion,0];
FinFuncion

// Dadas dos posiciones verifica que las cartas sean iguales.
// @param cartas: arreglo, el mazo.
// @param posicion1: entero, posición de una carta para comparar.
// @param posicion2: entero, posición de la otra carta para comparar.
// @return lógico.
Funcion igualdad <- son_iguales (cartas, posicion1, posicion2)
	Definir igualdad Como Logico;
	igualdad <- seleccionar_carta(cartas, posicion1) == seleccionar_carta(cartas, posicion2);
FinFuncion


// Intercambia de lugar un par de cartas.
// @param cartas: cadena, el mazo de cartas.
// @param posicion1: entero, posición de una carta para intercambiar.
// @param posicion2: entero, posición de otra carta para intercambiar por la primera.
SubProceso intercambiar_carta (cartas, posicion1, posicion2)
	Definir  cara, visible, imagen Como Caracter;
	cara    <- cartas[posicion1, 0];
	visible <- cartas[posicion1, 1];
	imagen  <- cartas[posicion1, 2];
	
	cartas[posicion1,0] <- cartas[posicion2,0];
	cartas[posicion1,1] <- cartas[posicion2,1];
	cartas[posicion1,2] <- cartas[posicion2,2];

	cartas[posicion2,0] <- cara;
	cartas[posicion2,1] <- visible;
	cartas[posicion2,2]	<- imagen;
FinSubProceso


// Mezcla un mazo de cartas.
// @param cartas: cadena, un mazo de cartas para mezclar.
// @param total_cartas: entero, la cantidad de cartas en el mazo.
SubProceso mezclar_cartas (cartas, total_cartas)
	Definir contador_mezcla, posicion1, posicion2 Como Entero;
	contador_mezcla <- total_cartas * 100;
	Repetir
		posicion1 <- azar (total_cartas);
		posicion2 <- azar (total_cartas);
		intercambiar_carta(cartas, posicion1, posicion2);
		contador_mezcla <- contador_mezcla -1;
	Hasta Que contador_mezcla == 0;
FinSubProceso


// Obtiene un mazo de cartas.
// @param cartas: arreglo cadena, mazo de cartas.
// @param total: entero: total de cartas en el mazo.
// @param tamx: entero, tamaño de la carta en eje x.
// @param tamy: entero, tamaño de la carta en eje y.
SubProceso obtener_cartas (cartas, total, tamx, tamy)
	Definir carta como Caracter;
	Repetir
		Escribir "Elegir una carta: ", total;
		leer carta;
		Si carta <> "" Entonces
			total <- total -2;
			cartas[total, 0] <- carta;
			cartas[total, 1] <- carta;
			cartas[total, 2] <- crear_imagen_carta (tamx, tamy, carta);

			cartas[total +1, 0] <- cartas[total, 0];
			cartas[total +1, 1] <- cartas[total, 1];
			cartas[total +1, 2] <- cartas[total, 2];
		SiNo
			Escribir "Ese valor no es válido";
		FinSi

	Hasta que total == 0;
FinSubProceso








// ---------------------
// vista ---------------
// ---------------------

// Devuelve una cadena que contiene una representación de la imagen de una carta.
// @param tamx: entero, ancho de la carta.
// @param tamy: entero, alto de la carta.
// @param carta: caracter, el valor de la carta.
// @return cadena.
Funcion imagen_carta <- crear_imagen_carta (tamx, tamy, carta)
	Definir imagen_carta, dibujo Como Caracter;
	imagen_carta <- "";
	
	Definir contadorx, contadory Como Entero;
	contadorx <- 0;
	contadory <- 0;

	// Evita desborde.
	carta <- Subcadena (carta, 0, tamx -2);
	
	Repetir		
		Repetir
			dibujo <- " ";
			
			Si redon(tamy / 2) -1 == contadory y redon(tamx/2) -1 == contadorx Entonces
				dibujo <- carta;
				contadorx <- contadorx + Longitud (carta) -1;
			FinSi
			
			Si contadorx == 0 o contadorx == tamx -1 Entonces
				dibujo <- "|";
			FinSi
			Si contadory == 0 o contadory == tamy -1 Entonces
				dibujo <- "-";
			FinSi
			imagen_carta <- Concatenar(imagen_carta,  dibujo );
			contadorx <- contadorx +1;
		Hasta Que contadorx == tamx; 
		contadory <- contadory +1;
		contadorx <- 0;
	Hasta Que contadory == tamy;
FinFuncion


// Crea una cadena de texto que representa una pantalla.
// @param cartas: arreglo, un mazo de cartas.
// @param total_cartas: entero, cantidad de cartas en el mazo.
// @param tamx: entero, tamaño de las cartas en eje x.
// @param tamy: entero, tamaño de las cartas en eje y.
// @param cartas_x: cantidad de cartas en eje x.
// @return cadena.
Funcion imagen_pantalla <- crear_imagen_pantalla (cartas, total_cartas, tamx, tamy, cartas_x)
	Definir imagen_pantalla Como Caracter;
	Definir cartas_desde, cartas_hasta como Entero;
	imagen_pantalla <- "";
	cartas_desde <- 0;
	cartas_hasta <- cartas_x;
	Repetir
		imagen_pantalla <- Concatenar (imagen_pantalla, crear_imagen_pantalla_x (cartas, cartas_desde, cartas_hasta,  tamx, tamy));
		cartas_desde <- cartas_desde + cartas_x;
		cartas_hasta <- cartas_desde + cartas_x;
		Si cartas_hasta > total_cartas Entonces
			cartas_hasta <- total_cartas;
		FinSi;
	Hasta Que cartas_desde >= total_cartas;

FinFuncion

Funcion imagen_pantalla <- crear_imagen_pantalla_x (cartas, cartas_desde, cartas_hasta, tamx, tamy)
	Definir imagen_pantalla, imagen, linea Como Caracter;
	imagen_pantalla <- "";

	Definir contador_carta, contadory Como Entero;
	
	contadory <- 0;
	Repetir		
		contador_carta <- cartas_desde;
		Repetir
			imagen <- cartas[contador_carta, 2];
			linea <- Subcadena(imagen, contadory * tamx, (contadory * tamx) + tamx-1);

			imagen_pantalla <- Concatenar (imagen_pantalla, "*");
			imagen_pantalla <- Concatenar (imagen_pantalla, linea);
			imagen_pantalla <- Concatenar (imagen_pantalla, "*");
			contador_carta <- contador_carta +1;
		Hasta Que contador_carta >= cartas_hasta;
		
		contadory <- contadory +1;
	Hasta Que tamy == contadory;
		

FinFuncion


// Imprime en pantalla una imagen.
// @param imagen: cadena, texto que contiene la imagen.
// @param tamx: entero, tamaño en eje x de la imagen.
SubProceso dibujar (imagen, tamx)
	Definir contadory Como Entero;
	contadory <- 0;
	Definir linea Como Caracter;
	Repetir
		linea <- Subcadena(imagen, contadory * tamx, (contadory * tamx) + tamx-1); 
		Escribir linea;
		contadory <- contadory +1;
	Hasta Que linea == "";
FinSubProceso



// provisorio
SubProceso dibujar_pantalla (cartas, datos_pantalla)
	Definir total_cartas, tamx, tamy, cartas_x Como Entero;
	total_cartas <- datos_pantalla[0];
	tamx         <- datos_pantalla[1];
	tamy         <- datos_pantalla[2];
	cartas_x     <- datos_pantalla[3];

	Definir imagen Como caracter;
	imagen <- crear_imagen_pantalla (cartas, total_cartas, tamx, tamy,cartas_x);
	
	Limpiar Pantalla;
	dibujar(imagen, (tamx+2)*cartas_x);

	Escribir "";
FinSubProceso




// ---------------------
// usuario -------------
// ---------------------


// Usuario selecciona una carta.
// @param cartas: cadena, mazo de cartas.
// @return entero.
SubProceso posicion<- elegir_carta (cartas)
	Definir posicion Como Entero;
	Definir carta Como Caracter;
	
	Repetir
		Escribir Sin Saltar "Seleccionar una carta: ";
		leer posicion;
		posicion <- posicion -1;
		Si esta_oculta(cartas, posicion) == Falso Entonces
			Escribir "Esa carta o posición no es correcta";
		FinSi
	Hasta Que esta_oculta(cartas, posicion);

	carta <- seleccionar_carta(cartas, posicion);	
	Escribir "Carta seleccionada: ", carta;
FinSubProceso





// Mecánica de juego.
SubProceso jugar (cartas, datos_pantalla)

		
	Definir total_cartas, posicion1, posicion2 Como Entero;
	total_cartas <- datos_pantalla [0];

		
	Repetir
		dibujar_pantalla(cartas, datos_pantalla);
		posicion1 <- elegir_carta(cartas);
		hacer_visible_carta(cartas, posicion1);		

		dibujar_pantalla(cartas, datos_pantalla);
		posicion2 <- elegir_carta(cartas);
		hacer_visible_carta(cartas, posicion2);
	
		dibujar_pantalla(cartas, datos_pantalla);

		Si son_iguales(cartas, posicion1, posicion2) Entonces
			Escribir "Correcto";
		SiNo
			Escribir "¡NO!";
			hacer_oculta_carta(cartas, posicion1);
			hacer_oculta_carta(cartas, posicion2);
			Esperar 6 segundos;
		FinSi

		Si son_todas_visibles(cartas, total_cartas) Entonces
			Escribir "¡GANASTE!";
		FinSi
		Si posicion2 > total_cartas Entonces
			hacer_visible_cartas(cartas, total_cartas);
			Escribir "adios";
		FinSi

	Hasta Que son_todas_visibles(cartas, total_cartas);
	
FinSubProceso








Proceso juego_memoria
	Definir total_cartas, tamx, tamy, cartas_x Como Entero;

	tamx <- 6;
	tamy <- 6;
	cartas_x <- 3;

	Escribir "Total de cartas";
	leer total_cartas;
	total_cartas <- total_cartas *2;


	Definir cartas Como Caracter;
	Dimension cartas(total_cartas,3);


	Definir datos_pantalla Como Entero;
	Dimension datos_pantalla(10);
	datos_pantalla[0] <- total_cartas;
	datos_pantalla[1] <- tamx;
	datos_pantalla[2] <- tamy;
	datos_pantalla[3] <- cartas_x;

	
	obtener_cartas(cartas, total_cartas, tamx, tamy);
	
	dibujar_pantalla(cartas, datos_pantalla);
	Escribir "cartas elegidas";
	Esperar 2 segundos;

	mezclar_cartas(cartas, total_cartas);

	dibujar_pantalla(cartas, datos_pantalla);
	Escribir "Memorizar...";
	Esperar 10 segundos;

	Definir tiempo_espera Como Entero;
	Para tiempo_espera <- 5 Hasta 0 con paso -1 Hacer
		dibujar_pantalla(cartas, datos_pantalla);
		Escribir "Empieza en ", tiempo_espera;
		Esperar 1 segundos;
	FinPara
	
	hacer_oculta_cartas(cartas, total_cartas);

	
	


	jugar(cartas, datos_pantalla);
FinProceso
